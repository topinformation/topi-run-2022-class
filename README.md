# ![TopiRun!](/assets/images/topirun-icon.png "TopiRun") Seja Bem Vindo !

Esse repo esta separado em aulas, de forma cronológica, de modo que você pode trocar de branch para ver a evolução do código e criar o seu próprio.

## Estrutura de branches

### master 
Representa o estado inicial, de propósito, para que vc possa criar o seu repositório seguido a sequencia de aulas. Cada aula esta separada conforme o exemplo abaixo

* feature/aula-01


##  :bulb:  Como instalar os códigos diretamente no Salesforce
--------

Execute os comandos abaixo no terminal 


``` bash

sfdx force:auth:web:login -a my-org-nickname --instanceurl https://login.salesforce.com

sfdx force:mdapi:deploy -d ./src --verbose -u my-org-nickname

```

:eyes: ``` my-org-nickname ``` é o apelido que você deu para sua Org

<br><br><br>

## Referêcias Técnicas 

### :books: Dica de Livros 

--------

Segue abaixo todas as literaturas citadas durante o curso

* Clean Code
* Elements of reusable object-oriented software (GOF Design Patterns)
* Domain Driven Design - DDD
* Working Effectively with a legacy Code
* Growing Object-Oriented Software, Guided by Tests
* [Enterprise Integration Patterns] (https://www.enterpriseintegrationpatterns.com/)

<br>

### :rocket: Padrões Apresentados

Segue abaixo alguns padrões de projeto comentados e alguns aplicados durante o curso

<br>

* Repository (DDD - Domain Driven Design - Eric Evans)
* Singleton  (GOF Design Patterns - Erich Gamma)
* Factory (GOF Design Patterns - Erich Gamma)
* FixtureFactory (TDD - Kent Back)
* TriggerHanlder (Salesforce)
* Builder - (GOF Design Patterns - Erich Gamma)
* Filter - (Bisso mesmo kkk )
* Enricher - ( Nada mais é que um Builder )

<br><br>

### :boom: Open your mind :boom:
------
Segue abaixo um conjunto de livros que me ajudaram a esponencializar o meu conhecimento técnico

* Mindeset
* Garra
* Milagre da Manhã
* Poder do Hábito
* Micro Hábitos
* Roube como um artista
* 7 Hábitos de pessoas altamente eficazes
* Segredos de uma mente milionária
* Mais esperto que o Diabo
* Quem pensa enriquece
* Seja Foda
* 100% Presente
* Fora de Série - Outliers
* Ponto da Virada
* Blink
* Arrume sua cama