/**
 * @author ercarval - topi/brq 
 */
public abstract class Customer {

    //modificadores Tipo nomeVariavel;
    //
    //private 
    //public 
    //protected

    // ----- atributos ----- bad comment

    protected String name;

    String email;
    Integer age;
    
    Date birthDate;
    
    Datetime createdDate;
    
    Decimal totalAmount;
   
    List<Contact> contacts;
    Set<Address> addresses;
    Map<String, Address> addressesByRegion;
    Map<String, List<Address>> addressesGroupedByRegion;

    public Customer() {

        Case caze = new Case();

        contacts = new List<Contact> {
            new Contact (FirstName  = 'Bisso', Email = 'bisso@mail.com')
            , new Contact (FirstName  = 'Bissolino')
            , new Contact (FirstName  = 'Edu')
            , new Contact (FirstName  = 'Du')
        };

        addresses = new Set<Address> {
            new Address ()
            , new Address ()
            , new Address ()
        };


    }


    public Customer(String name) {
        this();
        this.name = name;
    }

    public Customer(String name, String email) {
        this(name);
        this.email = email;
    }


    public String getName() {
        return this.name;
    }

    virtual
    public void print () {

        System.debug('Customer: ' + this.name);

    }

    public Boolean isValid () {
        return this.name == null;
    }


    public Decimal getTotalAmount() {
        return this.totalAmount;
    }

}

